import styled from 'styled-components';

export const ListNextContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 16px;
`;
