import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { fromWei } from 'web3-utils';

import { setWalletBalance } from './reducer';

import { getUserAddress, useAppSelector } from 'store';

import { captureError } from 'utils/errors';

export function useQVault () {
  const dispatch = useDispatch();

  const walletBalance = useAppSelector(({ qVault }) => qVault.walletBalance);

  async function loadWalletBalance () {
    try {
      const balance = await window.web3.eth.getBalance(getUserAddress());
      dispatch(setWalletBalance(fromWei(balance)));
    } catch (error) {
      captureError(error);
    }
  }

  async function loadAllBalances () {
    try {
      await loadWalletBalance();
    } catch (error) {
      captureError(error);
    }
  }

  return {
    walletBalance,
    loadWalletBalance: useCallback(loadWalletBalance, []),
    loadAllBalances: useCallback(loadAllBalances, []),
  };
}
