import styled from 'styled-components';

export const ApproveTipWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
  align-items: center;
  gap: 15px;
`;
