import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface QVaultState {
  walletBalance: string;
}

const initialState: QVaultState = {
  walletBalance: '0',
};

const qVaultSlice = createSlice({
  name: 'q-vault',
  initialState,
  reducers: {
    setWalletBalance (state, { payload }: PayloadAction<string>) {
      state.walletBalance = payload;
    }
  }
});

export const { setWalletBalance } = qVaultSlice.actions;
export default qVaultSlice.reducer;
