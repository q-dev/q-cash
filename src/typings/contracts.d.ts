import { ContractRegistryInstance, SystemContractWithQBalance } from '@q-dev/q-js-sdk';
import { BaseContractInstance } from '@q-dev/q-js-sdk/lib/contracts/BaseContractInstance';
import { LiquidationAuctionInstance } from '@q-dev/q-js-sdk/lib/contracts/defi/LiquidationAuctionInstance';
import { SystemDebtAuctionInstance } from '@q-dev/q-js-sdk/lib/contracts/defi/SystemDebtAuctionInstance';
import { SystemSurplusAuctionInstance } from '@q-dev/q-js-sdk/lib/contracts/defi/SystemSurplusAuctionInstance';
import { ConstitutionInstance } from '@q-dev/q-js-sdk/lib/contracts/governance/constitution/ConstitutionInstance';
import { EPDRParametersInstance } from '@q-dev/q-js-sdk/lib/contracts/governance/experts/EPDRParametersInstance';
import { EPQFIParametersInstance } from '@q-dev/q-js-sdk/lib/contracts/governance/experts/EPQFIParametersInstance';
import { EPRSParametersInstance } from '@q-dev/q-js-sdk/lib/contracts/governance/experts/EPRSParametersInstance';

export type AuctionInstance = LiquidationAuctionInstance | SystemDebtAuctionInstance | SystemSurplusAuctionInstance;

type KeyOfType<T, U> = {
  [P in keyof T]: T[P] extends U ? P: never
}[keyof T];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ContractPromise = Promise<BaseContractInstance<any> | SystemContractWithQBalance[]>;
export type ContractType = KeyOfType<ContractRegistryInstance, (val: string) => ContractPromise>;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ContractValue<T extends ContractType = any> = ReturnType<ContractRegistryInstance[T]>;

export type ParametersInstance =
 | ConstitutionInstance
 | EPQFIParametersInstance
 | EPRSParametersInstance
 | EPDRParametersInstance;
