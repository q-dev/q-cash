import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import { Asset } from 'typings/defi';

import ErrorBoundary from 'components/Custom/ErrorBoundary';
import Auction from 'pages/Auctions/components/Auction';
import NewAuction from 'pages/Auctions/components/NewAuction';
import DataPrivacy from 'pages/DataPrivacy';
import Imprint from 'pages/Imprint';
import NotFound from 'pages/NotFound';
import SavingBorrowing from 'pages/SavingBorrowing';
import BorrowingPair from 'pages/SavingBorrowing/BorrowingPair';

import Auctions from '../pages/Auctions';
import Dashboard from '../pages/Dashboard';
import Manage from '../pages/Parameters';

import { RoutePaths } from 'constants/routes';

function Routes () {
  return (
    <ErrorBoundary>
      <Switch>
        <Route exact path={['/', '/dashboard/:slug']}>
          <Dashboard />
        </Route>

        <Route exact path="/parameters">
          <Manage />
        </Route>

        <Route exact path={RoutePaths.savingBorrowingTab}>
          <SavingBorrowing />
        </Route>

        <Route
          exact
          path={RoutePaths.borrowingPair}
          component={(props: RouteComponentProps<{
            collateral: Asset;
            borrow: 'QUSD';
          }>) => (
            <BorrowingPair {...props} />
          )}
        />

        <Route exact path="/auctions/:type?">
          <Auctions />
        </Route>

        <Route exact path="/auctions/:type/new">
          <NewAuction />
        </Route>

        <Route
          exact
          path="/auction/:type?/:slug?"
          component={Auction}
        />

        <Route exact path="/imprint">
          <Imprint />
        </Route>
        <Route exact path="/data-privacy">
          <DataPrivacy />
        </Route>

        <Route component={NotFound} />
      </Switch>
    </ErrorBoundary>
  );
}

export default Routes;
