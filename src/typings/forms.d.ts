export type Option<T> = {
  value: T;
  label: string;
};
export type Options<T> = Option<T>[];
export type RadioOptions<T> = (Option<T> & { tip?: string })[];
