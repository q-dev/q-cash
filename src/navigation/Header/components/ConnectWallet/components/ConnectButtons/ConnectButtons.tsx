import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { WalletType } from 'connectors';
import { useWeb3Context } from 'context/Web3ContextProvider';

import Button from 'ui/Button';

function ConnectButtons () {
  const { t } = useTranslation();

  const { connectWallet, success, loading, error, setError } = useWeb3Context();

  useEffect(() => {
    return () => {
      setError(null);
    };
  }, []);

  if (success) {
    return (
      <div className="connect">
        <h5>{t('SUCCESS')}</h5>
        <p>{t('REFRESHING_THE_PAGE')}</p>
      </div>
    );
  }

  if (loading) {
    return <div className="connect-loading">{t('LOADING')}</div>;
  }

  if (error) {
    return (
      <div className="connect">
        <p>{t('ERROR_WHILE_CONNECTING_TO_WALLET')}</p>
      </div>
    );
  }

  if (!window.ethereum) {
    return (
      <div className="connect">
        <p>{t('NO_WALLETS_CONNECTED')}</p>
      </div>
    );
  }

  return (
    <div className="connect_buttons">
      {window.ethereum.isMetaMask && (
        <Button
          alwaysEnabled
          style={{ width: '100%' }}
          onClick={() => connectWallet(WalletType.INJECTED, true)}
        >
          <img
            src="/icons/metamask.svg"
            alt="metamask"
            className="icon"
          />
          <span>{t('CONNECT_WITH_METAMASK')}</span>
        </Button>
      )}

      <Button
        alwaysEnabled
        style={{ width: '100%' }}
        onClick={() => connectWallet(WalletType.COINBASE, true)}
      >
        <img
          src="/icons/coinbase.png"
          alt="metamask"
          className="icon"
        />
        <span>{t('CONNECT_WITH_COINBASE')}</span>
      </Button>

      {/* TODO: add bridge between dApp and connect to wallet
       <Button
        alwaysEnabled
        style={{ width: '100%' }}
        onClick={() => connectWallet(WalletType.WALLET_CONNECT, true)}
      >
        Connect with Wallet Connect
      </Button> */}
    </div>
  );
}

export default ConnectButtons;
