import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import PageLayout from 'components/PageLayout';
import Button from 'ui/Button';
import Icon from 'ui/Icon';

import QEPDRParameters from './components/QEPDRParameters';

function ManageParameters () {
  const { t } = useTranslation();

  return (
    <PageLayout
      title={t('PARAMETERS')}
      action={
        <div style={{ display: 'flex', gap: '16px' }}>
          <Link to="/">
            <Button
              block
              alwaysEnabled
              look="secondary"
            >
              <Icon name="dashboard" />
              <span>Dashboard</span>
            </Button>
          </Link>
        </div>
      }
    >
      <QEPDRParameters />
    </PageLayout>
  );
}

export default ManageParameters;
