import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import styled from 'styled-components';
import { media } from 'styles/media';

import PageLayout from 'components/PageLayout';
import Button from 'ui/Button';
import Icon from 'ui/Icon';

import useNetworkConfig from 'hooks/useNetworkConfig';

import BalanceOverview from './components/BalanceOverview';
import InterestRateBlock from './components/InterestRateBlock';

import { useInterestRates } from 'store/borrowing/hooks';

const StyledWrapper = styled.div`
  .dashboard__main {
    display: grid;
    gap: 24px;

    ${media.lessThan('medium')} {
      gap: 16px;
    }
  }

  .dashboard-rates {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    gap: 24px;

    ${media.lessThan('medium')} {
      grid-template-columns: 1fr;
      gap: 16px;
    }
  }
`;

function Dashboard () {
  const { t } = useTranslation();
  const { collaterals } = useNetworkConfig();
  const { interestRates, getInterestRates } = useInterestRates();

  useEffect(() => {
    getInterestRates(collaterals);
  }, []);

  return (
    <StyledWrapper>
      <PageLayout
        title={t('DASHBOARD')}
        action={
          <Link to="/parameters">
            <Button block alwaysEnabled>
              <Icon name="list" />
              <span>{t('PARAMETERS')}</span>
            </Button>
          </Link>
        }
      >
        <div className="dashboard__main">
          <BalanceOverview />
          <div className="dashboard-rates">
            {interestRates.map(rate => (
              <InterestRateBlock key={rate.asset} rate={rate}/>
            ))}
          </div>
        </div>
      </PageLayout>
    </StyledWrapper>
  );
}

export default Dashboard;
