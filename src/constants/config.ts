import { Asset } from 'typings/defi';
import Web3 from 'web3';

export type NetworkName = 'mainnet' | 'testnet';

interface NetworkConfig {
  chainId: number;
  name: string;
  networkName: NetworkName;
  rpcUrl: string;
  indexerUrl: string;
  explorerUrl: string;
  gnosisSafeUrl: string;
  qBridgeUrl: string;
  docsUrl: string;
  constitutionUrl: string;
  constitutionUpdatedAt: number;
  collaterals: Asset[];
  featureFlags: {
    aliases: boolean;
  };
}

interface ConnectorParams {
  chainId: string;
  chainName: string;
  rpcUrls: string[];
  blockExplorerUrls: string[];
  nativeCurrency: {
    name: string;
    symbol: string;
    decimals: number;
  };
}

export const networkConfigsMap: Record<NetworkName, NetworkConfig> = {
  mainnet: {
    chainId: 35441,
    name: 'Q Mainnet',
    networkName: 'mainnet',
    rpcUrl: 'https://rpc.q.org',
    indexerUrl: 'https://indexer.q.org',
    explorerUrl: 'https://explorer.q.org',
    gnosisSafeUrl: 'https://multisig-ui.q.org',
    qBridgeUrl: 'https://bridge.q.org',
    docsUrl: 'https://docs.q.org',
    constitutionUrl: 'https://constitution.q.org',
    constitutionUpdatedAt: 1657120401000,
    collaterals: ['QBTC'],
    featureFlags: { aliases: false },
  },
  testnet: {
    chainId: 35443,
    name: 'Q Testnet',
    networkName: 'testnet',
    rpcUrl: 'https://rpc.qtestnet.org',
    indexerUrl: 'https://indexer.qtestnet.org',
    explorerUrl: 'https://explorer.qtestnet.org',
    gnosisSafeUrl: 'https://multisig-ui.qtestnet.org',
    qBridgeUrl: 'https://bridge.qtestnet.org',
    docsUrl: 'https://docs.qtestnet.org',
    constitutionUrl: 'https://constitution.qtestnet.org',
    constitutionUpdatedAt: 1657120425000,
    collaterals: ['QBTC', 'QUSDC', 'QDAI'],
    featureFlags: { aliases: false },
  },
};

export const chainIdToNetworkMap: { [key: string]: NetworkName } = {
  35441: 'mainnet',
  35443: 'testnet',
};

export const connectorParametersMap = Object.values(networkConfigsMap)
  .reduce((acc, config) => {
    acc[config.chainId] = {
      chainId: Web3.utils.toHex(config.chainId).replace('0x', ''),
      chainName: config.name,
      rpcUrls: [config.rpcUrl],
      blockExplorerUrls: [config.explorerUrl],
      nativeCurrency: {
        name: 'Q',
        // HACK: MetaMask requires the symbol to have at least 2 characters
        symbol: 'Q ',
        decimals: 18,
      },
    };
    return acc;
  }, {} as { [key: string]: ConnectorParams });

const originToNetworkMap: { [key: string]: NetworkName } = {
  'https://qcash.org': 'mainnet',
  'http://localhost:3000': 'testnet',
};

export const ORIGIN_NETWORK_NAME: NetworkName = originToNetworkMap[window.location.origin] || 'testnet';
