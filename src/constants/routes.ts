export enum RoutePaths {
  dashboard = '/',
  auctions = '/auctions',

  liquidation = '/auctions/liquidation',
  systemDebt = '/auctions/system-debt',
  systemSurplus = '/auctions/system-surplus',

  newLiquidation = '/auctions/liquidation/new',
  newSystemDebt = '/auctions/system-debt/new',
  newSystemSurplus = '/auctions/system-surplus/new',

  qParameters = '/parameters',

  savingBorrowing = '/saving-borrowing',
  savingBorrowingTab = '/saving-borrowing/:tab?',
  saving = '/saving-borrowing/saving',
  borrowing = '/saving-borrowing/borrowing',
  borrowingPair = '/saving-borrowing/borrowing/:collateral-:borrow',
}
