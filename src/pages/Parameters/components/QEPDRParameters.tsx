import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import DefiMembersPanel from './DefiMembersPanel';
import ParametersBlock from './ParametersBlock';

import { useParameters } from 'store/parameters/hooks';

import { getEpdrParametersInstance } from 'contracts/contract-instance';

const StyledWrapper = styled.div`
  display: grid;
  gap: 24px;
`;

function QEPDRParameters () {
  const { t } = useTranslation();
  const {
    epdrParameters,
    epdrParametersLoading,
    epdrParametersError,
    getEpdrParameters
  } = useParameters();

  const [ePDRParametersAddress, setEPDRParametersAddress] = useState('0x00');

  useEffect(() => {
    getEpdrParameters();
    getEpdrParametersInstance().then((contract) => setEPDRParametersAddress(contract.address));

    return () => {
      setEPDRParametersAddress('0x00');
    };
  }, []);

  return (
    <StyledWrapper>
      <ParametersBlock
        title={t('Q_DEFI_RISK_EXPERT_PANEL_PARAMETERS')}
        subtitle={`(${ePDRParametersAddress})`}
        docsId="#q-defi-risk-expert-panel-epdr-parameters"
        parameters={epdrParameters}
        loading={epdrParametersLoading}
        errorMsg={epdrParametersError}
      />

      <DefiMembersPanel />
    </StyledWrapper>
  );
}

export default QEPDRParameters;
